Fabricator(:article) do
  from { "TW" }
  resolved_url { "https://www.google.com" }
  status { "unread" }
end
