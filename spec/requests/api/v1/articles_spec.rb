require 'rails_helper'

describe "Articles API" do

  let(:json) { JSON.parse(response.body) }
  let(:status) { response.status }
  let(:path) { '/path' }
  let(:auth_token) { '1234' }
  let(:user) { Fabricate(:user) }
  let(:article1) { Fabricate(:article) }
  let(:article2) { Fabricate(:article) }
  let(:article3) { Fabricate(:article) }
  let(:auth_headers) do
    {
      'HTTP_AUTHTOKEN' => auth_token,
      'CONTENT_TYPE' => 'application/json',
      'ACCEPT' => 'application/json'
    }
  end

  before do
    get path, nil, auth_headers
  end

  describe 'GET /api/v1/users/{user_id}/articles' do
    let(:path) { "/api/v1/users/#{user.id}/articles" }

    it do
      expect(status).to eq(200)
      expect(json).to eq([])
    end
  end
end
