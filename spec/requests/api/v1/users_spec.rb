require 'rails_helper'

describe "Users API" do

  let(:json) { JSON.parse(response.body) }
  let(:status) { response.status }
  let(:path) { '/path' }
  let(:params) { {} }
  let(:auth_token) { '1234' }
  let(:auth_headers) do
    {
      'HTTP_AUTHTOKEN' => auth_token,
      'CONTENT_TYPE' => 'application/json',
      'ACCEPT' => 'application/json'
    }
  end

  before do
    post path, params.to_json, auth_headers
  end

  describe 'POST /api/v1/authenticate' do
    let(:path) { "/api/v1/authenticate" }

    context 'good params' do
      let(:params) do
        {
          networkID: '1234',
          network: 'TW',
          accessToken: '12415gq',
          secretToken: 'h31ooioth3i1o'
        }
      end

      subject { User.last }

      it do
        expect(status).to eql(200)
        expect(json).to eq({ 'userID' => subject.id, 'accessToken' => subject.access_token })
      end
    end

    context 'bad params' do
      it { expect(status).to eql(422) }
    end
  end
end
