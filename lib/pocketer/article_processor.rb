class Pocketer::ArticleProcessor

  class Twitter

    def initialize(user)
      @user = user
    end

    def process
      tweets = client.favorites(include_entities: true)
      tweets.each do |tweet|

        unless tweet.urls.first
          puts 'skip, no urls'
          next
        end

        attributes = {
          resolved_url: [tweet.urls.first.expanded_uri],
          status: :unread,
          word_count: -1,
          excerpt: tweet.full_text,
          network: network_name,
          network_id: tweet.id
        }
      end
    end

    private

    def network_name
      'TW'
    end

    def client
      @client ||= ::Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TW_CONSUMER_KEY']
        config.consumer_secret     = ENV['TW_CONSUMER_SECRET']
        config.access_token        = passport.access_token
        config.access_token_secret = passport.secret_token
      end
    end

    def passport
      @passport ||= Passport.where(user_id: @user.id, network: 'TW').first
    end
  end

  class Pocket

    def initialize(user)
      @user = user
      ::Pocket.configure do |config|
        config.consumer_key = ENV['POCKET_CONSUMER_KEY'] || '46552-7a2ca9fac9dd1e0feb3fe4a0'
      end
    end

    def process
      data = client.retrieve(state: :all, contentType: :article, detailType: :complete, count: 100)
      raise PocketerError.new('Failed to process pocket articles') unless data
      data['list'].each_pair do |id, article|
        if Article.exists?(network: network_name, network_id: article['resolved_id'])
          next
        end
        attributes = {
          resolved_url: article['resolved_url'],
          status: :unread,
          word_count: article['word_count'],
          excerpt: article['excerpt'],
          network: network_name,
          network_id: article['resolved_id']
        }
        attributes.merge!(main_image: article['image']['src']) if article['image']
        article = Article.create!(attributes)
        @user.articles << article
      end
    end

    private

    def network_name
      'POCKET'
    end

    def client
      @client ||= ::Pocket.client(:access_token => passport.access_token)
    end

    def passport
      @passport ||= Passport.where(user_id: @user.id, network: 'POCKET').first
    end
  end
end

