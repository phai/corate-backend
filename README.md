# Pocketer Backend

## API's

# POST /api/v1/authenticate

Authenticate the user

# POST /api/v1/users/:user_id/process

Process twitter/pocket articles

# GET /api/v1/users/:user_id/articles

Get 3 selected articles

p/s: For more details how to use it, please check the pocketer app.

## HowTo Run

### Set the envs

| Env | Remarks | Example |
|-----|---------|---------|
| POCKET_CONSUMER_KEY | Pocket consumer key  | 46552-7a2ca9fac9dd1e0feb3fe4a0 |
| TW_CONSUMER_KEY     | Twitter Consumer key | Xnd7qgM9n1epACsXQdzyLkQpB      |
| TW_CONSUMER_SECRET= | Twitter Consumer secret | OV1ufMj6GCbmWI87ghsgrQOvKsmSNT2WYGkygkcc9MtpJxIfu5 |

### Run the server

```
bundle install
bundle exec rails s
```

