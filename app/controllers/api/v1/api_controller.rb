class Api::V1::ApiController < ApplicationController
  before_filter :check_format
  skip_before_action :verify_authenticity_token
  rescue_from Exception, with: :render_error

  skip_before_filter :verify_authenticity_token

  protected

  def current_user
    @current_user
  end

  def authenticate_token
    auth_token = request.headers['HTTP_AUTHTOKEN']
    @current_user ||= User.find_by(access_token: auth_token, id: params['user_id'])
    raise PocketerError.new(401, 'Invalid auth token') unless current_user
  end

  def render_error(error)
    puts error.message
    puts error.backtrace
    status = begin
               error.status
             rescue
               422
             end

    render json: { message: error.message }, status: status
  end

  def check_format
    render text: text_unsupported_format, status: :unsupported_media_type unless request.format == :json
  end

  def text_unsupported_format
    "It only supports json format, please check the documentation!</a>"
  end
end
