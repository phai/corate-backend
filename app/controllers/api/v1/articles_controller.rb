class Api::V1::ArticlesController < Api::V1::ApiController

  def index
    authenticate_token
    @articles = Article.where(selected: true)
  end
end
