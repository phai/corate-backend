class Api::V1::UsersController < Api::V1::ApiController

  def authenticate
    # Find if passport exist
    passport = Passport.where(network_id: params['networkID'], network: params['network']).first
    passport_attributes = {
      network_id: params['networkID'],
      network: params['network'],
      access_token: params['accessToken'],
      secret_token: params['secretToken']
    }

    if passport
      passport.update_attributes(passport_attributes)
      @user = User.find(passport.user_id)
    else
      User.transaction do
        @user = User.create!
        @user.update_attributes!(username: "user_#{@user.id}")
        @user.passports << Passport.create!(passport_attributes)
      end
    end
    @user.update_attributes(access_token: User.generate_token)
  end

  def process_articles
    authenticate_token
    current_user.passports.each do |passport|
      if passport.network == 'TW'
        Pocketer::ArticleProcessor::Twitter.new(current_user).process
      elsif passport.network == 'POCKET'
        Pocketer::ArticleProcessor::Pocket.new(current_user).process
      end
    end
    Article.unmark_selected_article
    (1..3).each do
      Article.rand_article
    end
  end
end
