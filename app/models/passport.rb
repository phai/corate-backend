class Passport < ActiveRecord::Base
  module NetworkType
    TW = 'TW'
    POCKET = 'POCKET'
  end

  validates :access_token, presence: true
  validates :network, inclusion: [ NetworkType::TW, NetworkType::POCKET ]

  belongs_to :user
end
