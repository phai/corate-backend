class Article < ActiveRecord::Base

  STATUS = [ 'read', 'unread','archived' ].freeze

  validates :status, presence: true, inclusion: STATUS

  has_many :article_tags
  has_many :tags, through: :article_tags

  def self.unmark_selected_article
    Article.where(selected: true).update_all(selected: false)
  end

  def self.rand_article
    offset = rand(Article.count)
    rand_record = Article.where(status: 'unread').offset(offset).limit(1).first
    rand_record.update_attributes(selected: true) if rand_record
  end
end
