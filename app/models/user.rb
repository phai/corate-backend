class User < ActiveRecord::Base
  has_many :passports, dependent: :destroy
  has_many :articles

  def self.generate_token(size = 178)
    SecureRandom.base64(size).tr('+/=', '0aZ').first(size)
  end

end
