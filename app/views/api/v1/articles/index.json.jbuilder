json.array! @articles.each do |article|
  json.partial! 'article', article: article
end
