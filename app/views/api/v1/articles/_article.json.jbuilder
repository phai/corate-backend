json.userID article.user_id
json.url article.resolved_url
json.status article.status
json.image article.main_image
json.wordCount article.word_count
json.excerpt article.excerpt
json.network article.network
