Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      post '/authenticate' => "users#authenticate", as: 'authenticate'
      resources :users, only: [] do
        post '/process' => "users#process_articles", as: 'process'
        resources :articles, only: [:index]
      end
    end
  end

end
