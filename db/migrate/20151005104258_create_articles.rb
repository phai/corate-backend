class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :user_id
      t.string :from
      t.string :resolved_url
      t.string :status

      t.timestamps null: false
    end
  end
end
