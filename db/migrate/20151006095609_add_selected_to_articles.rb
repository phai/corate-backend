class AddSelectedToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :selected, :boolean, default: false
  end
end
