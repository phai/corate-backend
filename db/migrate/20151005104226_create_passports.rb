class CreatePassports < ActiveRecord::Migration
  def change
    create_table :passports do |t|
      t.integer :user_id
      t.string :access_token
      t.string :secret_token
      t.string :username
      t.string :network_type

      t.timestamps null: false
    end
  end
end
