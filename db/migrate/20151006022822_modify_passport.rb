class ModifyPassport < ActiveRecord::Migration
  def up
    add_column :passports, :network_id, :string, null: false
    add_column :passports, :network, :string, null: false
  end
end
