class ModifyArticle < ActiveRecord::Migration
  def change
    add_column :articles, :main_image, :string
    add_column :articles, :word_count, :integer
    add_column :articles, :excerpt, :string
    add_column :articles, :network, :string
    add_column :articles, :network_id, :string
  end
end
