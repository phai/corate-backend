class ArticleTagRelationships < ActiveRecord::Migration
  def change
    create_table :article_tag_relationship do |t|
      t.integer :article_id
      t.integer :tag_id

      t.timestamps null: false
    end
  end
end
